package hotel.entities;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.api.function.Executable;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;

import hotel.credit.CreditCard;

import static org.junit.jupiter.api.Assertions.*;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

@ExtendWith(MockitoExtension.class)

class TestBookingHotel {
	ServiceType serviceType=ServiceType.ROOM_SERVICE;
	int cost;
	 Date arrivalDate;
	 int stayLength;
	 int occupantNumber;
	@Mock CreditCard mockCard;
	ServiceCharge serviceCharge;
	@Mock Guest mockGuest;
	@Mock Room mockRoom;
	@Spy List<ServiceCharge> charges=new ArrayList<>();
	long confirmationNumber;
	int roomId;
	@Mock Booking booking;
	@InjectMocks Hotel hotel;
	@Spy Map<Integer,Booking> activeBookingsByRoomId=new HashMap<Integer,Booking>();
	
	SimpleDateFormat format=new SimpleDateFormat("dd-MM-yyyy");
	
	@BeforeEach
	void setUp() throws Exception {
		 format=new SimpleDateFormat("dd-MM-yyyy");
		 arrivalDate=format.parse("11-12-2001");
		 stayLength=1;
		 occupantNumber=1;
		 confirmationNumber=11122001202L;
		 roomId=202;
		 booking = new Booking(mockGuest, mockRoom, arrivalDate,  stayLength,occupantNumber, mockCard );
		 ServiceType serviceType=ServiceType.ROOM_SERVICE;
		 cost=20;
	}

	@AfterEach
	void tearDown() throws Exception {
	}

	@Test
	public void testHotelBookingAddServiceCharges() {
		//Adding service charge from the class Hotel by Calling
		//addServiceCharge method of class Booking
		charges=booking.getCharges();
		
		//Checking if size of charges in booking object is 0
		assertEquals(0,charges.size());
		
		//Executing the method in Booking class
		booking.addServiceCharge(serviceType,cost);
		
		//Checking the size of charges after method execution
		charges=booking.getCharges();
		assertEquals(1,charges.size());

		//Checking the ServiceType of the first charge in the list
		assertEquals(ServiceType.ROOM_SERVICE,charges.get(0).getType());
		
		//Checking the cost of the first charge in the list
		assertEquals(20,charges.get(0).getCost());
		
		}
	}
