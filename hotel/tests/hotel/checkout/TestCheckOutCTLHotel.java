package hotel.checkout;

import static org.junit.jupiter.api.Assertions.*;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.api.function.Executable;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;

import hotel.credit.CreditCard;
import hotel.entities.Booking;
import hotel.entities.Guest;
import hotel.entities.Hotel;
import hotel.entities.Room;
import hotel.entities.ServiceCharge;
import hotel.entities.ServiceType;

@ExtendWith(MockitoExtension.class)
class TestCheckOutCTLHotel {
	ServiceType serviceType=ServiceType.ROOM_SERVICE;
	int cost;
	 Date arrivalDate;
	 int stayLength;
	 int occupantNumber;
	@Mock CreditCard mockCard;
	ServiceCharge serviceCharge;
	@Mock Guest mockGuest;
	@Mock Room mockRoom;
	long confirmationNumber;
	int roomId;
    @Mock Booking mockBooking;
	@InjectMocks Hotel hotel;
	@Spy Map<Integer,Booking> activeBookingsByRoomId=new HashMap<Integer,Booking>();
	List<Booking> bookings;
	
	//@Spy Map<Integer,Booking> mockBookingsByRoomId=new HashMap<Integer,Booking>();
	
	SimpleDateFormat format=new SimpleDateFormat("dd-MM-yyyy");

	@BeforeEach
	void setUp() throws Exception {
		 format=new SimpleDateFormat("dd-MM-yyyy");
		 arrivalDate=format.parse("19-12-2001");
		 stayLength=1;
		 occupantNumber=1;
		 confirmationNumber=19112001201L;
		 roomId=201;
	}


	@AfterEach
	void tearDown() throws Exception {
	}

	@Test
	void testHotelCheckOut() {
		
		//Checking if the particular roomId has a corresponding booking
		assertEquals(null,hotel.findActiveBookingByRoomId(roomId));
		assertEquals(0,hotel.activeBookingsByRoomId.size());
		
		//Putting a booking with the room id in the map 
		hotel.activeBookingsByRoomId.put(roomId, mockBooking);
		
		//Checking if the size of the map has been changed by the above command
		assertEquals(1,hotel.activeBookingsByRoomId.size());
		assertEquals(mockBooking,hotel.findActiveBookingByRoomId(roomId));
		
		//Calling the method checkout to remove the booking and roomid from the map
		hotel.checkout(roomId);
		
		//Checking if the booking has been removed from the map
		assertEquals(0,hotel.activeBookingsByRoomId.size());
		assertEquals(null,hotel.findActiveBookingByRoomId(roomId));
		
	}

}
